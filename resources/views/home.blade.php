@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <ul class="list-group">
                        <li style="cursor: pointer;" class="list-group-item"><a href="{{ route('pacientes.index')}}">Pacientes</a></li>
                        <li style="cursor: pointer;" class="list-group-item"><a href="{{ route('examens.index')}}">Examenes</a></li>
                        <li style="cursor: pointer;" class="list-group-item"><a href="{{ route('estudios.index')}}">Estudios</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
