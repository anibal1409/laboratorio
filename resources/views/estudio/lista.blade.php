@extends('layouts.master')
@section('contenedor')
<div  style="margin: 4%;">
  <div class="row">
    <div class="col-xs-12">
        <table class="table" >
          <thead class="thead-light">
            <tr>
              <th scope="col">Paciente</th>
              <th scope="col">Examenes</th>
              <th scope="col">Fecha</th>
              <th scope="col">Estado</th>
              <th scope="col">Acciones</th>
            </tr>
          </thead>
          <tbody>
              @foreach ($estudios as $estudio)
              <tr>
                  <td>
                        {{$estudio->paciente['nombre'].' '.$estudio->paciente['apellido']}}
                  </td>
                  <td>
                        {{sizeof($estudio->resultados)}}
                  </td>
                  <td>
                        {{date_format($estudio->created_at, 'd/m/Y g:i A')}}
                  </td>
                  <td>
                       {{$estudio->estado}}
                  </td>
                  <td style="display: inline-flex;">
                        <a href="{{ url('estudios/'.$estudio->id.'/edit')}}" class="btn btn-primary">Ver/Editar</a>
                        <form method="POST" action="{{ route('estudios.destroy', $estudio->id)}}">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit">
                                Eliminar
                            </button>
                        </form>
                        <a href="{{ url('estudios_crear/'.$estudio->paciente['id'])}}" class="btn btn-success">Paciente</a>
                    </td>
              </tr>
              @endforeach
          </tbody>
        </table>
    </div>
  </div>
  <div class="row">
        <div class="col-xs-6">
            <a href="{{ route('home')}}">
                <button class="btn btn-danger" onclick="{{ route('home')}}">Regresar</button>
            </a>
        </div>
        <div class="col-xs-6">
          <a href="{{ route('estudios.create')}}">
            <button class="btn btn-success" >Registrar</button>
          </a>
        </div>
  </div>
</div>
@endsection
