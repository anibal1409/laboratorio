@extends('layouts.master')
@section('contenedor')
<div class="form-group" style="margin: 4%;">
    <div class="row">
        <div class="col-xs-3">
            <label for="dni">Documento de identidad</label>
            <input type="text" name="dni" id="dni" class="form-control" value="{{isset($paciente)?  $paciente->dni : old('dni')}}"
            @if (isset($paciente))
            disabled
            @endif>
            <strong class="text-danger" id="errordni"></strong>
            <strong class="text-danger">{{ $errors->first('paciente') }}</strong>
        </div>
        <div class="col-xs-1" style="align-self: end;">
            @if (!isset($paciente))
                <button type="button" id="buscar"class="btn btn-success">Buscar</button>
            @endif
        </div>
        <div class="col-xs-5">
            <label for="nombre">Nombre y Apellido</label>
            <input type="text" name="nombre" id="nombre" class="form-control" value="{{isset($paciente)?  $paciente->nombre.' '.$paciente->apellido : ''}}" disabled>
        </div>
        <div class="col-xs-1">
            <label for="apellido">Edad</label>
            <input type="text" name="edad" id="edad" class="form-control" value="{{isset($paciente)?  $paciente->edad() : ''}}" disabled>
        </div>
        <div class="col-xs-2">
            <label for="telefono">Telefono</label>
            <input type="text" name="telefono" id="telefono" class="form-control" value="{{isset($paciente)?  $paciente->telefono : ''}}" disabled>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-5">
            <label for="telefono">Examen</label>
            <select name="examen" id="examen" class="form-control">
                <option value="Seleccione un examen">Seleccione un examen</option>
                @foreach($examenes as $examen)
                    <option value="{{$examen->id}}" {{$examen->estado=='DISPONIBLE'? '' : 'disabled'}}>{{$examen->nombre}}</option>
                @endforeach
            </select>
            <strong class="text-danger" id="errorExamen"></strong>
        </div>
        <div class="col-xs-2" style="align-self: center;">
            <a href="javascript:;" id="agregar" class="btn btn-success">Agregar</a>
        </div>
        <div class="col-xs-5">
            <label for="telefono">Estado</label>
            <select name="estado" id="estado" class="form-control">
                <option value="Por realizar">Por realizar</option>
                <option value="Realizado">Realizado</option>
                <option value="Impreso">Impreso</option>
                <option value="Entregado">Entregado</option>
            </select>
            <strong class="text-danger" >{{ $errors->first('estado') }}</strong>
        </div>
    </div
    </form>
    <form method="POST" action="{{isset($estudio) ? route('estudios.update', $estudio->id) : route('estudios.store')}}" class="form-group">
            @csrf
            @isset($estudio)
                @method('PUT')
            @endisset
            <input type="hidden" name="paciente" id="paciente" value="{{isset($paciente) ? $paciente->id : ''}}">
            <div class="row">
                <div class="col-xs-12">
                    <table class="table" id="tabla">
                        <thead class="thead-light">
                        <tr>
                            <th scope="col">Examen</th>
                            <th scope="col">Valor</th>
                            <th scope="col">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                            @isset($resultados)
                                @foreach ($resultados as $resultado)
                                <tr id="fila{{$resultado->examen_id}}">
                                    <input type="hidden" name="estudio[{{$resultado->id}}][resultado]" id="ev{{$resultado->examen_id}}" value="{{$resultado->id}}">
                                    <input type="hidden" name="estudio[{{$resultado->id}}][examen]" id="ev{{$resultado->examen_id}}" value="{{$resultado->examen_id}}">
                                    <td><input class="form-control" type="text" name="estudio[{{$resultado->id}}][examenTexto]" value="{{$resultado->examen['nombre']}}" readonly></td>
                                    <td><input class="form-control" type="text" name="estudio[{{$resultado->id}}][valor]" value="{{$resultado->valor}}"></td>
                                    <td><button type="button" class="btn btn-danger" onClick="Eliminar({{$resultado->examen_id}}, {{$resultado->id}})">Eliminar</button></td>
                                </tr>
                                @endforeach
                            @endisset
                        </tbody>
                    </table>
                    <strong class="text-danger">{{ $errors->first('estudio') }}</strong>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <a href="{{ route('estudios.index')}}">
                        <button type="button" class="btn btn-danger" >Cancelar</button>
                    </a>
                </div>
                <div class="col-xs-6">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </div>
            <div style="display:" id="eliminados"

            </div>

        </form>
</div>

@endsection
@section('fin')
<script type="text/javascript">
    let table = $('#tabla');
    let eliminados = $('#eliminados');
    let tableFila = $('#tabla >tr');
    let examenes = [];
    let editando = false;
    $('#agregar').click(function(){
        agregar();
    });
    /*$('#tblUsuario tr').each(function () {

    }*/
    $.each($('input'),function(i,val){
        if($(this).attr("type")=="hidden"){
            let valor = $(this).attr("id") + '';
            //let cadena = valor.split('-',2);
            //console.log(valor, $(this).attr("name"), $(this).attr("value"));
            if(valor === 'ev'+$(this).attr("value")) {
                Selector($(this).attr("value"), false);
                if (!editando) editando = true;
                console.log(valor, $(this).attr("name"), $(this).attr("value"));
            }
            /*if(cadena.length>1) {

                $(this).attr("value")
            }*/
            //console.log(cadena);
            //var valueOfHidFiled=$(this).attr("id");
            //console.log(valueOfHidFiled, $(this).attr("name"), $(this).attr("value"));
        }
    });
    $('#buscar').click(function(){
        Buscar();
    });
       /* $("td").each(function(){
            console.log($(this).find("input:hidden").attr('value'));
            if('estudio[6][examen]' === $(this).children("input").attr('name')) {
                console.log($(this).children("input").attr('value'));
            }
        });*/
    //change()

    function agregar() {
        let examen=$('#examen').val();
        //console.log('Indice', examenes.indexOf(examen), examen);
        if(examenes.indexOf(examen) === -1 && examen != null && examen != 'Seleccione un examen') {
            //examenes.push(examen);
            //let valor=$('#valor').val();
            let examenTexto=$('#examen option:selected').text();
            Selector(examen, false);
            //$("#examen option[value=" +examen + "]").hide();
            //let bloqueo=$('#examen option:selected').prop('disabled', 'disabled');
            //$('select[name="lineas"] option:selected').text());
            let i = $('#tabla tr').length ;
            //console.log(examenTexto);
            let fila='<tr id="fila'+examen+'">'+
            '<input type="hidden" name="estudio['+examen+'][resultado]"  value="null">'+
            '<input type="hidden" name="estudio['+examen+'][examen]" id="ev'+examen+'" value="'+examen+'">'+
            '<td><input class="form-control" type="text" name="estudio['+examen+'][examenTexto]" value="'+examenTexto+'" readonly></td>'+
            '<td><input class="form-control" type="text" name="estudio['+examen+'][valor]" value="" ></td>'+
            '<td><button type="button" class="btn btn-danger" onClick="Eliminar('+examen+', null)">Eliminar</button></td>'+
            '</tr>';
            //console.log(examenes);
            table.append(fila);
            $('#examen').val('Seleccione un examen');
            $('#errorExamen').text('');
           // $("#examen >option[value='Seleccione un examen']").attr("selected", true);
            //$("#examen >option[value='Seleccione un examen']").attr("selected", false);
        }
        else {
            $('#errorExamen').text('Debe seleccionar un examen válido.');
        }
    }

    function Eliminar(i, resultado) {

        //let vid = '#ev'+i;
        console.log('indice', i );
        let valorExamen=  i+'';//$('#ev'+i).val();
        /*console.log('valorExamen', valorExamen);*/
        console.log('examenes', examenes);
        console.log(examenes.indexOf(valorExamen));
        //examenes.splice(examenes.indexOf(valorExamen), 1);

        Selector(valorExamen, true);
        console.log('examenes', examenes);
        if (editando && resultado != null) {
            let fila= '<input type="hidden" name="eliminados['+resultado+'][examen]" id="ee'+resultado+'" value="'+resultado+'">';
            eliminados.append(fila);
        }
        //$("#examen option[value=" +valorExamen + "]").show();
        $('#fila'+i).remove();
        //document.getElementById("tabla").deleteRow(i);
        //tableFila.remove(i);
    }

    function Selector(valor, estado) {
        if(estado){
            $("#examen option[value=" + valor + "]").show();
            examenes.splice(examenes.indexOf(valor), 1);
        }
        else {
            $("#examen option[value=" + valor + "]").hide();
            examenes.push(valor);
        }
    }

    function Buscar() {
        $.ajax({
            // En data puedes utilizar un objeto JSON, un array o un query string
            //data: {$('#dni').val()},
            //Cambiar a type: POST si necesario
            type: "GET",
            // Formato de datos que se espera en la respuesta
            dataType: "json",
            // URL a la que se enviará la solicitud Ajax
            url: "http://127.0.0.1:8000/pacientes/"+$('#dni').val(),
        })
         .done(function( data, textStatus, jqXHR ) {
             console.log(jqXHR);
             if(jqXHR.responseJSON.length > 0) {
                 paciente = jqXHR.responseJSON[0];
                $('#paciente').val(paciente.id);
                $('#nombre').val(paciente.nombre+' '+paciente.apellido);
                $('#edad').val(Edad(paciente.fecha));
                $('#telefono').val(paciente.telefono);
             } else {
                $('#errordni').text('Doc. de identidad no registrado.');
                $('#paciente').val(null);
             }

         })
         .fail(function( jqXHR, textStatus, errorThrown ) {
             if ( console && console.log ) {
                 console.log( "La solicitud a fallado: " +  textStatus);
             }
        });
    }

    function Edad(FechaNacimiento) {
        var fechaNace = new Date(FechaNacimiento);
        var fechaActual = new Date()
        var mes = fechaActual.getMonth();
        var dia = fechaActual.getDate();
        var año = fechaActual.getFullYear();
        fechaActual.setDate(dia);
        fechaActual.setMonth(mes);
        fechaActual.setFullYear(año);
        edad = Math.floor(((fechaActual - fechaNace) / (1000 * 60 * 60 * 24) / 365));
        return edad;
    }
</script>

@endsection
