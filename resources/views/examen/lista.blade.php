@extends('layouts.master')
@section('contenedor')
<div  style="margin: 4%;">
  <div class="row">
    <div class="col-xs-12">
        <table class="table" >
          <thead class="thead-light">
            <tr>
              <th scope="col">Examen</th>
              <th scope="col">Descripción</th>
              <th scope="col">Estado</th>
              <th scope="col">Acciones</th>
            </tr>
          </thead>
          <tbody>
              @foreach ($examenes as $examen)
              <tr>
                  <th scope="row">{{$examen->nombre}}</th>
                  <td>{{$examen->descripcion}}</td>
                  <td>{{$examen->estado}}</td>
                  <td style="display: inline-flex;">
                      <a href="{{ url('examens/'.$examen->id.'/edit')}}" class="btn btn-primary">Editar</a>
                      <form method="POST" action="{{ route('examens.destroy', $examen->id)}}">
                          @csrf
                          @method('DELETE')
                          <button class="btn btn-danger" type="submit">
                              Eliminar
                          </button>
                      </form>
                  </td>
              </tr>
              @endforeach
          </tbody>
        </table>
    </div>
  </div>
  <div class="row">
        <div class="col-xs-6">
            <a href="{{ route('home')}}">
                <button class="btn btn-danger" onclick="{{ route('home')}}">Regresar</button>
            </a>
        </div>
        <div class="col-xs-6">
          <a href="{{ route('examens.create')}}">
            <button class="btn btn-success" >Registrar</button>
          </a>
        </div>
  </div>
</div>
@endsection
