@extends('layouts.master')
@section('contenedor')
<div class="form-group" style="margin: 4%;">
    <form action="{{ isset($examen)? route('examens.update', $examen->id) : route('examens.store') }}" method="POST">
    @csrf
    @if (isset($examen))
        @method('PUT')
    @endif
    <div class="row">
        <div class="col-xs-12">
            <label for="nombre">Nombre</label>
            <input type="text" name="nombre" id="nombre" class="form-control" value="{{isset($examen)?  $examen->nombre : ''}}">
            <strong class="text-danger">{{ $errors->first('nombre') }}</strong>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <label for="apellido">Descripción</label>
            <textarea name="descripcion" id="descripcion"  rows="3" class="form-control">{{isset($examen)?  $examen->descripcion : ''}}</textarea>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
        <label for="apellido">Estado</label>
            <select name="estado" id="estado" class="form-control">
                <option value="DISPONIBLE">DISPONIBLE</option>
                <option value="NO DISPONIBLE">NO DISPONIBLE</option>
                <option value="FALTA MATERIAL">FALTA MATERIAL</option>
                <option value="OTRO">OTRO</option>
            </select>
        </div>
    </div>
    <div class="row">
            <div class="col-xs-6">
                <a href="{{ route('examens.index')}}">
                    <button type="button" class="btn btn-danger" >Cancelar</button>
                </a>

            </div>
        <div class="col-xs-6">
            <button type="submit" class="btn btn-primary">Guardar</button>
        </div>
    </div>
    </form>
</div>
@endsection
