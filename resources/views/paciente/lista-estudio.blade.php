@extends('layouts.master')
@section('contenedor')
<div  style="margin: 4%;">
  <div class="row">
    <div class="col-xs-3">
        <label for="dni">Documento de identidad</label>
        <input type="text" name="dni" id="dni" class="form-control" value="{{  isset($paciente)?  $paciente->dni : old('dni')}}" readonly>
    </div>
    <div class="col-xs-8">
        <label for="nombre">Nombre y Apellido</label>
        <input type="text" name="nombre" id="nombre" class="form-control" value="{{isset($paciente)?  $paciente->nombre.' '.$paciente->apellido : old('nombre')}}" readonly>
    </div>
    <div class="col-xs-1">
      <label for="nombre">Edad</label>
      <input type="text" name="edad" id="edad" class="form-control" value="{{isset($paciente)?  $paciente->edad(): old('edad')}}" readonly>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-4">
      <label for="dni">Teléfono</label>
      <input type="text" name="telefono" id="telefono" class="form-control" value="{{  isset($paciente)?  $paciente->telefono : old('telefono')}}" readonly>
    </div>
    <div class="col-xs-4">
      <label for="dni">E-mail</label>
      <input type="text" name="email" id="email" class="form-control" value="{{  isset($paciente)?  $paciente->email : old('email')}}" readonly>
    </div>
    <div class="col-xs-4">
      <label for="dni">Dirección</label>
      <input type="text" name="direccion" id="direccion" class="form-control" value="{{  isset($paciente)?  $paciente->direccion : old('direccion')}}" readonly>
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="col-xs-12">
        <table class="table" >
          <thead class="thead-light">
            <tr>
              <th scope="col">Examenes</th>
              <th scope="col">Fecha</th>
              <th scope="col">Estado</th>
              <th scope="col">Acciones</th>
            </tr>
          </thead>
          <tbody>
              @isset($estudios)
                @foreach ($estudios as $estudio)
                <tr>
                    <td>
                            {{sizeof($estudio->resultados)}}
                    </td>
                    <td>
                            {{date_format($estudio->created_at, 'd/m/Y g:i A')}}
                    </td>
                    <td>
                        {{$estudio->estado}}
                    </td>
                    <td style="display: inline-flex;">
                            <a href="{{ url('estudios/'.$estudio->id.'/edit')}}" class="btn btn-primary">Ver/Editar</a>
                            <form method="POST" action="{{ route('estudios.destroy', $estudio->id)}}">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">
                                    Eliminar
                                </button>
                            </form>
                        </td>
                </tr>
                @endforeach
              @endisset
          </tbody>
        </table>
    </div>
  </div>
  <div class="row">
        <div class="col-xs-6">
            <a href="{{ route('home')}}">
                <button class="btn btn-danger" onclick="{{ route('home')}}">Regresar</button>
            </a>
        </div>
        <div class="col-xs-6">
          <a href="{{ route('estudios.create')}}">
            <button class="btn btn-success" >Registrar</button>
          </a>
        </div>
  </div>
</div>
@endsection
