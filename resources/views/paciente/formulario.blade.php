@extends('layouts.master')
@section('contenedor')
<div class="form-group" style="margin: 4%;">
    <form action="{{ ($paciente!= null)? url('pacientes/'.$paciente->id) : route('pacientes.store') }}" method="POST">
    @csrf
    @if ($paciente!= null)
        @method('PUT')
    @endif
    <div class="row">
        <div class="col-xs-12">
            <label for="dni">Documento de identidad</label>
            <input type="text" name="dni" id="dni" class="form-control" value="{{($paciente!= null)?  $paciente->dni : old('dni')}}">
            <strong class="text-danger">{{ $errors->first('dni') }}</strong>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <label for="nombre">Nombre</label>
            <input type="text" name="nombre" id="nombre" class="form-control" value="{{($paciente!= null)?  $paciente->nombre : ''}}">
            <strong class="text-danger">{{ $errors->first('nombre') }}</strong>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <label for="apellido">Apellido</label>
            <input type="text" name="apellido" id="apellido" class="form-control" value="{{($paciente!= null)?  $paciente->apellido : ''}}">
        </div>
    </div>
    <div class="row">
            <div class="col-xs-12">
                <label for="fecha">Fecha de nacimiento</label>
                <input type="date" name="fecha" id="fecha" class="form-control" style="width: 100%;" value="{{($paciente!= null)?  $paciente->fecha : ''}}">
            </div>
        </div>
    <div class="row">
        <div class="col-xs-12">
            <label for="email">E-mail</label>
            <input type="email" name="email" id="email" class="form-control" value="{{($paciente!= null)?  $paciente->email : ''}}">
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <label for="telefono">Telefono</label>
            <input type="text" name="telefono" id="telefono" class="form-control" value="{{($paciente!= null)?  $paciente->telefono : ''}}">
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <label for="direccion">Dirección</label>
            <input type="text" name="direccion" id="direccion" class="form-control" value="{{($paciente!= null)?  $paciente->direccion : ''}}">
        </div>
    </div>
    <div class="row">
            <div class="col-xs-6">
                <a href="{{ route('pacientes.index')}}">
                    <button type="button" class="btn btn-danger" >Cancelar</button>
                </a>

            </div>
        <div class="col-xs-6">
            <button type="submit" class="btn btn-primary">Guardar</button>
        </div>
    </div>
    </form>
</div>
@endsection
