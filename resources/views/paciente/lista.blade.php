@extends('layouts.master')
@section('contenedor')
<div  style="margin: 4%;">
  <div class="row">
    <div class="col-xs-12">
        <table class="table" border>
          <thead class="thead-light">
            <tr>
              <th scope="col">Doc. de identidad</th>
              <th scope="col">Nombre</th>
              <th scope="col">Apellido</th>
              <th scope="col">Fecha de Nac.</th>
              <th scope="col">Acciones</th>
            </tr>
          </thead>
          <tbody>
              @foreach ($pacientes as $paciente)
              <tr>
                <th scope="row">{{$paciente->dni}}</th>
                <td>{{$paciente->nombre}}</td>
                <td>{{$paciente->apellido}}</td>
                <td>{{$paciente->edad()}}</td>
                <td style="display: inline-flex;">
                  <a href="{{ url('pacientes/'.$paciente->id.'/edit')}}" class="btn btn-primary">Editar</a>
                  <form method="POST" action="{{ route('pacientes.destroy', $paciente->id)}}">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger" type="submit">
                      Eliminar
                    </button>
                  </form>
                  <a href="{{ route('estudios.paciente', $paciente->id)}}" class="btn btn-success">Estudios</a>
                </td>
              </tr>
              @endforeach
          </tbody>
        </table>
    </div>
  </div>
  <div class="row">
        <div class="col-xs-6">
            <a href="{{ route('home')}}">
                <button class="btn btn-danger" onclick="{{ route('home')}}">Regresar</button>
            </a>
        </div>
        <div class="col-xs-6">
          <a href="{{ route('pacientes.create')}}">
            <button class="btn btn-success" onclick="{{ route('pacientes.create')}}">Registrar</button>
          </a>
        </div>
  </div>
</div>
@endsection
