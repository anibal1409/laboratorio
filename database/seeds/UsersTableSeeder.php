<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Examen;
use App\Paciente;
use App\Resultado;
use App\Estudio;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' 			    =>	'Admin',
            'email'		        =>	'admin@admin.com',
            'usuario'           =>  'admin',
            'nivel'             =>  'ADMINISTRADOR',
            'password'          =>  bcrypt('1234'),
            'email_verified_at' => date("Y-m-d H:i:s")
       ]);

       $examen =Examen::create([
        'nombre' 			   =>	'examen 1',
        'estado'		       =>	'DISPONIBLE',
        'descripcion'          =>  'Prueba'
        ]);

        $paciente = Paciente::create([
            'dni' 			        =>	'23539583',
            'nombre' 			    =>	'Anibal',
            'apellido'		        =>	'Fariñas',
            'fecha'                 =>  '1999-09-14',
            'email' 			    =>	'anibal@gmail.com',
            'telefono'		        =>	'0412',
            'direccion'             =>  'Aqui'
        ]);

        $estudio= Estudio::create([
            'estado' 			    =>	'para entregar',
            'paciente_id'		    =>	$examen->id
        ]);

        $resultado= Resultado::create([
            'valor' 			    =>	'000',
            'estudio_id'		        =>	$estudio->id,
            'examen_id'                 =>  $examen->id
        ]);
    }
}
