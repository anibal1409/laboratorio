<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resultados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('valor')->nullable();
            $table->integer('estudio_id')->unsigned();
            $table->foreign('estudio_id')->references('id')->on('estudios')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('examen_id')->unsigned();
            $table->foreign('examen_id')->references('id')->on('examens')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resultados');
    }
}
