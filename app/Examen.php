<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Examen extends Model
{
    //
    use SoftDeletes;
    protected $fillable = [
        'nombre', 'estado', 'descripcion',
    ];

    public function resultados()
    {
        return $this->hasMany('App\Resultado');
    }
}
