<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Resultado extends Model
{
    //
    use SoftDeletes;
    protected $fillable = [
        'estudio_id', 'examen_id', 'valor',
    ];

    public function examen()
    {
        return $this->belongsTo('App\Examen', 'examen_id');
    }

    public function estudio()
    {
        return $this->belongsTo('App\Estudio', 'estudio_id');
    }
}
