<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Estudio extends Model
{
    //
    use SoftDeletes;
    protected $fillable = [
        'estado', 'paciente_id',
    ];

    public function paciente()
    {
        return $this->belongsTo('App\Paciente', 'paciente_id');
    }

    public function resultados()
    {
        return $this->hasMany('App\Resultado');
    }

    /*protected static function boot() {
        parent::boot();

        static::deleting(function($estudio) { // before delete() method call this
             $estudio->resultados()->delete();
             // do the rest of the cleanup...
        });
    }*/
}
