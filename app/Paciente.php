<?php

namespace App;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Paciente extends Model
{
    //
    use SoftDeletes;
    protected $fillable = [
        'nombre', 'apellido', 'fecha', 'email', 'telefono', 'direccion', 'dni',
    ];

    public function estudios()
    {
        return $this->hasMany('App\Estudio');
    }

    public function edad()
    {
        return Carbon::parse($this->fecha)->age;
    }
}
