<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Paciente;
use App\Examen;
use App\Resultado;
use App\Estudio;

class EstudioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //$estudios = Estudio::with('resultados')->with('paciente')->orderBy('nombre')->get();
        $estudios = Estudio::with('resultados')->with('paciente')->orderBy('created_at', 'desc')->get();
        //$estudios = Estudio::orderBy('created_at', 'desc')->get();
        //dd($estudios);

        return view('estudio.lista')->with('estudios', $estudios);
    }

    public function listaPaciente($paciente_id)
    {
        //
        $paciente = Paciente::findOrFail($paciente_id);
        //$estudios = Estudio::with('resultados')->with('paciente')->orderBy('nombre')->get();
        $estudios = Estudio::where('paciente_id',$paciente_id)->with('resultados')->orderBy('created_at', 'desc')->get();
        //$estudios = Estudio::orderBy('created_at', 'desc')->get();
        //dd($estudios);
        //dd($estudios);

        return view('paciente.lista-estudio')->with('estudios', $estudios)->with('paciente', $paciente);
    }

    public function crear($id)
    {
        //
        $paciente = Paciente::findOrFail($id);
        $examenes = Examen::orderBy('nombre')->get();
        // dd($paciente);
        return view('estudio.formulario')->with('paciente', $paciente)->with('examenes', $examenes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $examenes = Examen::orderBy('nombre')->get();
        return view('estudio.formulario')->with('examenes', $examenes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        /*$this->validate($request,
        [
         'dni'               => 'required|string|min:6|unique:pacientes|',
         'nombre'            => 'required|string|min:3|',
         'apellido'          => 'required|string|min:3|',
         'fecha'             => 'required|date|',
         'email'             => 'nullable|email|unique:pacientes|',
         'telefono'          => 'nullable|string|min:10|',
         'direccion'         => 'nullable|string|min:4'
        ],
     [
         'dni.unique' => 'Documento ya se encuentra registrado.'
     ]);*/
       $this->validate($request,
           [
            'paciente'           => 'required|string|min:1|',
            'estado'            => 'required|string|min:1|',
            'estudio'            => 'required|array|min:1',
            'estudio.*.examen'   => 'required|string|min:1|',
            'estudio.*.valor'   => 'nullable|string|min:1|'

           ],
        [
            'paciente.required' => 'Debe buscar un Doc. de identidad.',
            'estudio.required' => 'Debe agregar al menos un examen.'
        ]);

        $estudio = Estudio::create([
            'estado'        => $request->estado,
            'paciente_id'   => $request->paciente
        ]);
        //dd($estudio);

        foreach ($request->estudio as $estdios) {
            $resultados = Resultado::create([
                'estudio_id' => $estudio->id,
                'examen_id' => $estdios['examen'],
                'valor' => $estdios['valor']
            ]);
        }
        //dd($request);
        return redirect('estudios');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        //$paciente = Paciente::where('dni',$id)->get();
        $estudio = (Estudio::where('id',$id)->with('resultados')->get())[0];
        //dd($estudio->paciente_id);
        $paciente = Paciente::findOrFail($estudio->paciente_id);
        //dd($paciente);
        $resultados = array();
        foreach ($estudio->resultados as $result) {
            //dd($result->id);
            //dd((Resultado::where('id', $result->id)->with('examen')->get())[0]);
            $resultados[$result->id] = (Resultado::where('id', $result->id)->with('examen')->get())[0];
        }
        //dd($resultados);
        $examenes = Examen::orderBy('nombre')->get();
        // dd($paciente);
        return view('estudio.formulario')
        ->with('paciente', $paciente)
        ->with('examenes', $examenes)
        ->with('estudio', $estudio)
        ->with('resultados', $resultados);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //dd($request);
        if(is_null($id) ){
            return response()->json([
                'status'  =>'error',
                'message' =>$validator->errors(),
                'a'=>$request->all()
            ], 400);
        }
        //dd($id);
       $this->validate($request,
           [
            'paciente'              => 'required|string|min:1|',
            'estado'                => 'required|string|min:1|',
            'estudio'               => 'required|array|min:1',
            'estudio.*.resultado'   => 'nullable|string|min:1|',
            'estudio.*.examen'      => 'required|string|min:1|',
            'estudio.*.valor'       => 'nullable|string|min:1|',
            'eliminados'            => 'nullable|array|min:1',
            'eliminados.*.examen'   => 'required|string|min:1|',

           ],
        [
            'paciente.required' => 'Debe buscar un Doc. de identidad.',
            'estudio.required' => 'Debe agregar al menos un examen.'
        ]);

        $estudio = Estudio::findOrFail($id);
        //dd($estudio);
        $estudio->estado = $request->estado;
        //dd($estudio);
        $estudio->save();

        //dd($estudio);
        //dd($request->eliminados);
        if (isset($request->eliminados)) {
            foreach ($request->eliminados as $examen) {
                $resultado = Resultado::findOrFail($examen['examen']);
                $resultado->delete();
            }
        }
        //dd($request->estudio);
        foreach ($request->estudio as $estudios) {
            //dd($estudios['resultado']);
            if ($estudios['resultado'] === 'null') {
                //dd($estudios);
                $resultado = Resultado::create([
                    'estudio_id' => $id,
                    'examen_id' => $estudios['examen'],
                    'valor' => $estudios['valor']
                ]);
                //dd($resultado);
            }
            else {
                $resultado = Resultado::findOrFail($estudios['resultado']);
                //dd($resultado);
                $resultado->valor = $estudios['valor'];
                $resultado->save();
            }
        }

        return redirect('estudios');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if(is_null($id) ){
            return response()->json([
                'status'  =>'error',
                'message' =>$validator->errors(),
                'a'=>$request->all()
            ], 400);
        }
         $estudio = Estudio::findOrFail($id);
         //$estudio->resultados->delete();
         $estudio->delete();
         return redirect('estudios');
    }
}
