<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Examen;

class ExamenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $examenes = Examen::orderBy('nombre')->get();

        return view('examen.lista')->with('examenes', $examenes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('examen.formulario');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,
           [
            'nombre'               => 'required|string|min:2|unique:examens|',
            'descripcion'          => 'nullable|string|min:3|',
            'estado'                => 'required|string|'
           ],
        [
            'nombre.unique' => 'Este examen ya se encuentra registrado.'
        ]);

        $examen = Examen::create([
            'nombre' 			        =>	$request->nombre,
            'descripcion' 			    =>	$request->descripcion,
            'estado'		            =>	$request->estado
        ]);

        return redirect('examens');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $examen = Examen::findOrFail($id);

        return view('examen.formulario')->with('examen', $examen);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,
           [
            'nombre'               => 'required|string|min:2|unique:examens,nombre,'.$id,
            'descripcion'          => 'nullable|string|min:3|',
            'estado'               => 'required|string|'
           ],
        [
            'nombre.unique' => 'Este examen ya se encuentra registrado.'
        ]);

        $examen = Examen::findOrFail($id);
        $examen->fill($request->all());
        $examen->save();

        return redirect('examens');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if(is_null($id) ){
            return response()->json([
                'status'  =>'error',
                'message' =>$validator->errors(),
                'a'=>$request->all()
            ], 400);
        }
         $examen = Examen::findOrFail($id);
         $examen->delete();
         return redirect('examens');
    }
}
