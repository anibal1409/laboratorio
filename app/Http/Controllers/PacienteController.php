<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Paciente;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class PacienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $pacientes = Paciente::orderBy('nombre')->get();

        return view('paciente.lista')->with('pacientes', $pacientes);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $paciente = null;
        return view('paciente.formulario')->with('paciente', $paciente);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        /*$validator = Validator::make($request->all(), [
          'dni'               => 'required|string|min:6|unique:pacientes|',
          'nombre'            => 'required|string|min:3|',
          'apellido'          => 'required|string|min:3|',
          'fecha'             => 'required|date|',
          'email'             => 'nullable|email|unique:pacientes|',
          'telefono'          => 'nullable|string|min:10|',
          'direccion'         => 'nullable|string|min:4'
        ]);

        if($validator->fails()){
            return response()->json([
                'status'  =>'error',
                'message' =>$validator->errors(),
                'a'=>$request->all()
            ], 400);
        }*/
        $this->validate($request,
           [
            'dni'               => 'required|string|min:6|unique:pacientes|',
            'nombre'            => 'required|string|min:3|',
            'apellido'          => 'required|string|min:3|',
            'fecha'             => 'required|date|',
            'email'             => 'nullable|email|unique:pacientes|',
            'telefono'          => 'nullable|string|min:10|',
            'direccion'         => 'nullable|string|min:4'
           ],
        [
            'dni.unique' => 'Documento ya se encuentra registrado.'
        ]);

        $paciente = Paciente::create([
            'dni' 			        =>	$request->dni,
            'nombre' 			    =>	$request->nombre,
            'apellido'		        =>	$request->apellido,
            'fecha'                 =>  $request->fecha,
            'email' 			    =>	$request->email,
            'telefono'		        =>	$request->telefono,
            'direccion'             =>  $request->direccion
        ]);

        return redirect('pacientes');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $paciente = Paciente::where('dni',$id)->get();
        //dd($paciente);
    // if($paciente == null)
    //$paciente->fecha = Carbon::parse($paciente->fecha)->age;
    //dd($paciente);
        return $paciente;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $paciente = Paciente::findOrFail($id);
        return view('paciente.formulario')->with('paciente', $paciente);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(), [
            'dni'               => 'required|string|min:6|unique:pacientes,dni,'.$id,
            'nombre'            => 'required|string|min:3|',
            'apellido'          => 'required|string|min:3|',
            'fecha'             => 'required|date|',
            'email'             => 'nullable|email|unique:pacientes,email,'.$id,
            'telefono'          => 'nullable|string|min:10|',
            'direccion'         => 'nullable|string|min:4'
          ]);

          if($validator->fails()){
              return response()->json([
                  'status'  =>'error',
                  'message' =>$validator->errors(),
                  'a'=>$request->all()
              ], 400);
          }
        $paciente = Paciente::findOrFail($id);
        $paciente->fill($request->all());
        $paciente->save();

        return redirect('pacientes');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if(is_null($id) ){
            return response()->json([
                'status'  =>'error',
                'message' =>$validator->errors(),
                'a'=>$request->all()
            ], 400);
        }
         $paciente = Paciente::findOrFail($id);
         $paciente->delete();
         return redirect('pacientes');

    }
}
