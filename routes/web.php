<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true, 'register' => false]);

Route::middleware(['auth', 'verified'])->group(function () {

	Route::get('/home', 'HomeController@index')->name('home');
	// Route::get('/perfil', 'PerfilController@index')->name('perfil');
	Route::resource('examens', 'ExamenController');
    Route::resource('pacientes', 'PacienteController');
    Route::resource('resultados', 'ResultadoController');
    Route::resource('estudios', 'EstudioController');
    Route::get('/paciente/eliminar/{id}', 'PacienteController@destroy');
    Route::get('estudios_crear/{id}', 'EstudioController@crear')->name('estudios.crear');
    Route::get('estudios_paciente/{paciente}', 'EstudioController@listaPaciente')->name('estudios.paciente');


});
